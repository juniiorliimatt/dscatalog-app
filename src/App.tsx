import './App.css';
import './assets/styles/custom.scss';

import { MyRoutes } from 'routes';

const App = () => {
  return <MyRoutes />;
};

export default App;
