import './styles.css';

import ProductImg from 'assets/img/product.png';
import { ProductPrice } from 'components/ProductPrice';

export const ProductCard = () => {
  return (
    <>
      <div className="container base-card product-card">
        <div className="card-top-container">
          <img src={ProductImg} alt="Produto" />
        </div>
        <div className="card-botom-container">
          <h6>Nome do produto</h6>
          <ProductPrice />
        </div>
      </div>
    </>
  );
};
